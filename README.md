# Zines

A collection of translated or orignal zines. So far only in Czech!

Kolekce originálních, nebo přeložených zínů.

- [Biocentrická anarchie](https://gitlab.com/JindraZPrahy/Zines/-/blob/main/BiocentrickaAnarchie/bio.pdf) (originál)
- [Pro osvobozenou anarchii](https://gitlab.com/JindraZPrahy/Zines/-/tree/main/ProOsvobozenouAkademii) (překlad)
- [Život bez zákonů](https://gitlab.com/JindraZPrahy/Zines/-/blob/main/ZivotBezZakonu/law.pdf) (překlad)


## Licence

???
